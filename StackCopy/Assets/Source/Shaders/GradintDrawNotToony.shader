// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "InsideVisible" 
{
    Properties 
    {
        _BotYWCoord("_BotYWorldCoord", Float) = -1
        _TopYWCoord("_TopYWorldoord", Float) = 1
        _ColorBot ("_ColorBot (RGBA)", Color) = (1, 1, 1, 1) // add _Color property
        _ColorTop ("_ColorTop (RGBA)", Color) = (1, 1, 1, 1) // add _Color property
        _AmbientColor("_AmbientColor", Color) = (1,1,1,1)
    }

    SubShader 
    {
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "LightMode"="ForwardBase"}
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass 
        {
            CGPROGRAM
    
            #pragma target 3.0

            #pragma vertex vert alpha Standard fullforwardshadows
            #pragma fragment frag alpha Standard fullforwardshadows

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc" // I

            struct appdata_t 
            {
                float4 vertex   : POSITION;
		        float3 normal : NORMAL;
            };

            struct v2f 
            {
                float4 vertex  : SV_POSITION;
                fixed4 col : COLOR;
                float3 worldNormal : NORMAL;

                float alpha :Float;
            };



            float4 _ColorBot;
            float4 _ColorTop;
            float4 _AmbientColor;
            float _BotYWCoord;
            float _TopYWCoord;


            v2f vert (appdata_t v)
            {
                v2f o;
                float3 wPos= mul(unity_ObjectToWorld, v.vertex);
                o.vertex = UnityObjectToClipPos(v.vertex);

                float lerpVal = (wPos.y - _BotYWCoord) / (_TopYWCoord - _BotYWCoord);
                o.alpha = lerpVal ;
                float3 _Color = lerp(_ColorBot, _ColorTop, lerpVal);

		        o.worldNormal = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);

		        o.col = fixed4(_Color.rgb , lerpVal);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                 fixed4 col = i.col;
                 float3 normal = normalize(i.worldNormal);
                 float NdotL = dot(_WorldSpaceLightPos0, normal);

                 float shadow = LIGHT_ATTENUATION(i);
                 float lightIntensity = NdotL ;//* shadow ;  // SHADOW APPLIED HERE -----------------------------------------------
                 float4 light = lightIntensity * _LightColor0;

                 fixed4 finalCol =  col * (_AmbientColor + light);
                 finalCol.a = i.alpha;

                 return finalCol;
            }

            ENDCG
        }
    }
}
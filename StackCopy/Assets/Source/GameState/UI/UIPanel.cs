using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIPanel : MonoBehaviour
{
    public EPanel PanelType;

    private GameObject screen;

    private void Awake()
    {
        screen = transform.GetChild(0).gameObject;
    }

    internal void Open()
    {
        screen.gameObject.SetActive(true);
    }

    internal void Close()
    {
        screen.gameObject.SetActive(false);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private static UIPanel[] UIPanels;

    private void Awake()
    {
        UIPanels = GetComponentsInChildren<UIPanel>();
    }

    public static UIPanel ActivatePanel(EPanel ePanel)
    {
        UIPanel uIPanel = null;

        foreach (var item in UIPanels)
        {
            bool isTheOne = item.PanelType == ePanel;

            if (isTheOne)
            {
                item.Open();
            }
            else
            {
                item.Close();
            }

            if (isTheOne)
                uIPanel = item;
        }

        return uIPanel;
    }

    public static UIPanel ActivatePanelAdditionally(EPanel ePanel)
    {
        UIPanel uIPanel = null;

        foreach (var item in UIPanels)
        {
            bool isTheOne = item.PanelType == ePanel;

            if (isTheOne)
            {
                uIPanel = item;

                item.Open();

                break;
            }
        }

        return uIPanel;
    }

    public static void ClosePanel(EPanel ePanel)
    {
        foreach (var item in UIPanels)
        {
            bool isTheOne = item.PanelType == ePanel;

            if (isTheOne)
            {
                item.Close();

                break;
            }
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodManager : MonoBehaviour
{
    [SerializeField] private EGameState startState;

    [SerializeField] private GameConfig gameConfig;

    private GameData gameData;

    private PlayerData playerData;

    private static GameState[] gameStates;

    private static GameState activeGameState;

    private const string saveKey = "saveKey";

    private void Awake()
    {
        gameStates = GetComponentsInChildren<GameState>();

        gameData = new GameData();

        playerData = GetPlayer();

        foreach (var state in gameStates)
        {
            var systems = state.GetComponentsInChildren<GameSystem>();

            state.Systems = systems;

            foreach (var system in systems)
            {
                system.SetData(gameData, playerData, gameConfig);
            }

            state.SetUpState();
        }
    }

    private void Start()
    {
        ChangeGameState(startState);
    }

    private PlayerData GetPlayer()
    {
        return SaveExtension.Load(saveKey, new PlayerData());
    }

    public static void SavePlayer(PlayerData playerData)
    {
        SaveExtension.Save(playerData, saveKey);
    }

    public static void ChangeGameState(EGameState eGameState)
    {
        foreach (var item in gameStates)
        {
            if (item.EGameState == eGameState)
            {
                activeGameState = item;
            }
        }

        Debug.Log($"State Changed to: {eGameState}");

        activeGameState.OnInit();
    }

    private void Update()
    {
        if (activeGameState != null)
            activeGameState.OnUpdate();
    }

    private void FixedUpdate()
    {
        if (activeGameState != null)
            activeGameState.OnFixedUpdate();
    }
}

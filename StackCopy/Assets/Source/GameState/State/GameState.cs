using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    public EGameState EGameState;

    [HideInInspector] public GameSystem[] Systems;

    IIniting[] initings;

    IUpdating[] updatings;

    IFixedUpdating[] fixedUpdatings;

    public void SetUpState()
    {
        List<IIniting> initingsTmp = new List<IIniting>();

        List<IUpdating> updatingsTmp = new List<IUpdating>();

        List<IFixedUpdating> fixedUpdatingsTmp = new List<IFixedUpdating>();

        foreach (var item in Systems)
        {
            IIniting initing = item as IIniting;

            IUpdating iupdating = item as IUpdating;

            IFixedUpdating ifixedUpdating = item as IFixedUpdating;

            if (initing != null)
            {
                initingsTmp.Add(initing);
            }

            if (iupdating != null)
            {
                updatingsTmp.Add(iupdating);
            }

            if (ifixedUpdating != null)
            {
                fixedUpdatingsTmp.Add(ifixedUpdating);
            }
        }

        initings = initingsTmp.ToArray();

        updatings = updatingsTmp.ToArray();

        fixedUpdatings = fixedUpdatingsTmp.ToArray();
    }

    public void OnInit()
    {
        foreach (var item in initings)
        {
            item.OnInit();
        }
    }

    public void OnUpdate()
    {
        foreach (var item in updatings)
        {
            item.OnUpdate();
        }
    }

    public void OnFixedUpdate()
    {
        foreach (var item in fixedUpdatings)
        {
            item.OnFixedUpdate();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSystem : MonoBehaviour
{
    protected GameData game;

    protected PlayerData player;

    protected GameConfig config;

    public void SetData(GameData gameData, PlayerData playerData, GameConfig gameConfig)
    {
        game = gameData;

        player = playerData;

        config = gameConfig;
    }
}

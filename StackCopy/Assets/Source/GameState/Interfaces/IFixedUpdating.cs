
public interface IFixedUpdating 
{
    public void OnFixedUpdate();
}

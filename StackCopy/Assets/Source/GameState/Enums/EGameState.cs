
public enum EGameState 
{
    Loading = 1,
    Menu = 2,
    Game = 3,
    Result = 4,
    Settings = 5,
}

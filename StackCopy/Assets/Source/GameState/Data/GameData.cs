
using UnityEngine;

/// <summary>
/// data relative to a signle game session / not persistant
/// </summary>
public class GameData
{
    public GameStatus Status = new GameStatus();

    public Vector3 StartPos { get; internal set; }

    public ColorWheelToCol ColorWheelToCol { get; internal set; }

    public Color CurCol { get; internal set; }
    public GameObject LastCube { get; internal set; }
    public GameObject FirstCube { get; internal set; }
}

public class GameStatus
{
    public bool IsOver;

    public bool HasWon;
}


// data relative to a player which should be session persistant
public class PlayerData
{
    public int Money;

    public float Angle { get; internal set; }
}

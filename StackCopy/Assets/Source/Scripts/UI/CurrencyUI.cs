using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CurrencyUI : UIPanel
{
    [SerializeField] private TMP_Text currencyCntTmp;

    public void SetCurrencyCount(int cnt)
    {
        currencyCntTmp.text = cnt.ToString();
    }
}

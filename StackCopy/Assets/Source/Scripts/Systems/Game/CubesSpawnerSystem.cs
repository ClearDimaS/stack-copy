using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class CubesSpawnerSystem : GameSystem, IIniting, IUpdating
{
    [SerializeField] private Material matForCubes;

    [SerializeField] private float dstToNotTrim;

    [SerializeField] private Vector3 startSize;

    [SerializeField] private GameObject currentCube;

    [SerializeField] private float spawnOffset;

    private GameObject previousCube;

    private Vector3 currDir;

    private Vector3 currentSize;

    public float cubeSpeed;

    private float curSpeed;

    public float changeSpeed;

    public float angleChange;

    public float dstToBounceBack;

    private float sqrDistToBounceBack;

    Vector3 startPos;

    bool wasHere;

    private Vector3 originalSize;

    Vector2 startPSScale;

    public void OnInit()
    {
        originalSize = startSize;

        currDir = Vector3.right;

        currentCube.transform.localScale = startSize;

        game.FirstCube = currentCube;

        currentSize = startSize;

        curSpeed = cubeSpeed;

        startPSScale = new Vector2(currentCube.GetComponentInChildren<ParticleSystem>().main.startSizeXMultiplier, currentCube.GetComponentInChildren<ParticleSystem>().main.startSizeYMultiplier);

        ChangeDir();

        ChangeCube();

        UpdateColor(currentCube);

        sqrDistToBounceBack = dstToBounceBack * dstToBounceBack;
    }

    private void ChangeDir()
    {
        currDir = Quaternion.AngleAxis(90f, Vector3.up) * currDir;
    }

    private void ChangeCube()
    {
        previousCube = currentCube;

        currentCube = CreateCube(currentSize, -currDir * spawnOffset);

        curSpeed += changeSpeed;
    }

    public void OnUpdate()
    {
        if (wasHere == false)
        {
            wasHere = true;

            return;
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            OnCubeTryConnect();
        }
        else
        {
            currentCube.transform.position += curSpeed * currDir * Time.deltaTime;

            Vector3 diff = currentCube.transform.position - startPos;

            if ((diff).sqrMagnitude > sqrDistToBounceBack && Vector3.Dot(diff, currDir) > 0)
                currDir = -currDir;
        }
    }

    private GameObject CreateCube(Vector3 size, Vector3 pos)
    {
        var cubeGo = Instantiate(currentCube);

        cubeGo.name = "Created CUbe";

        UpdateColor(cubeGo);

        cubeGo.transform.position = pos + startPos + Vector3.up * startSize.y;

        currentSize = cubeGo.transform.localScale;

        return cubeGo;
    }

    private void OnCubeTryConnect()
    {
        Vector3 diff = currentCube.transform.position - previousCube.transform.position;

        diff.y = 0;

        float projectedDiff = Vector3.Dot(diff, currDir);

        if (Mathf.Abs(projectedDiff) > dstToNotTrim)
        {
            TrimCube(projectedDiff, diff);
        }
        else
        {
            PutOnTop();
        }

        startPos = currentCube.transform.position;

        ChangeDir();

        ChangeCube();

        if (game.Status.IsOver == false)
        {
            game.LastCube = currentCube;
        }

        game.StartPos = startPos;
    }

    private void TrimCube(float posDiffProj, Vector3 diff)
    {
        Vector3 startScale = currentCube.transform.localScale;

        if (Mathf.Abs(diff.x) < 0.01f)
            diff.x = 0;

        if (Mathf.Abs(diff.z) < 0.01f)
            diff.z = 0;

        Debug.Log($"{diff}   {posDiffProj}   { Mathf.Sign(diff.x)}   { Mathf.Sign(diff.z)}");//* Mathf.Sign(posDiffProj)

        Vector3 scaleChange = diff * Mathf.Sign(diff.z) * Mathf.Sign(diff.x);

        currentCube.transform.position -= diff / 2;// * Mathf.Sign(diff.z) * Mathf.Sign(diff.x);// * Mathf.Sign(posDiffProj);// * Mathf.Sign(currDir.x) * Mathf.Sign(currDir.z);

        currentCube.transform.localScale -= scaleChange;// * Mathf.Sign(diff.x) * Mathf.Sign(diff.z); // * Mathf.Sign(posDiffProj);//

        // create trimmed part

        Vector3 diff2 = new Vector3(Mathf.Abs(diff.x), Mathf.Abs(diff.y), Mathf.Abs(diff.z));

        Vector3 trimmedPartScale = Vector3.Scale(startScale, Vector3.one - diff2.normalized) + scaleChange;

        Vector3 trimmedPartPos = currentCube.transform.position - Vector3.Scale((currentCube.transform.localScale / 2f + trimmedPartScale / 2f), -diff.normalized);

        var trimmedGO = GameObject.CreatePrimitive(PrimitiveType.Cube);

        trimmedGO.GetComponent<MeshRenderer>().sharedMaterial = currentCube.GetComponent<MeshRenderer>().sharedMaterial;

        //UpdateColor(trimmedGO);

        trimmedGO.name = "trimmed";

        trimmedGO.transform.position = trimmedPartPos;

        trimmedGO.transform.localScale = trimmedPartScale;

        trimmedGO.AddComponent<Rigidbody>();

        if (currentCube.transform.localScale.x <= 0 || currentCube.transform.localScale.z <= 0)
        {
            currentCube.transform.localScale = new Vector3(0,
                0, Mathf.Max(currentCube.transform.localScale.z, 0));

            game.Status.IsOver = true;

            MMVibrationManager.Haptic(HapticTypes.SoftImpact);

            GodManager.ChangeGameState(EGameState.Result);
        }
    }

    private void PutOnTop()
    {
        currentCube.transform.position = previousCube.transform.position + Vector3.up * startSize.y;

        var ps = currentCube.GetComponentInChildren<ParticleSystem>();

        var main = ps.main;
        Debug.LogWarning($"{currentSize.x / originalSize.x}   {currentSize.z / originalSize.z}");

        main.startSizeYMultiplier = currentSize.z / originalSize.z * startPSScale.y;
        main.startSizeXMultiplier = currentSize.x / originalSize.x * startPSScale.x;


        ps.Play();

        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

    private void UpdateColor(GameObject cubeGo)
    {
        game.CurCol = game.ColorWheelToCol.GetColor(player.Angle);

        cubeGo.GetComponent<MeshRenderer>().material.SetColor("_Color", game.CurCol);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowerSystem : GameSystem, IIniting, IUpdating
{
    [SerializeField] private Transform follower;

    [SerializeField] private Transform target;

    [SerializeField] private float translateSpeed;

    private Vector3 startOffset;

    public void OnInit()
    {
        startOffset = follower.position - target.position;
    }

    public void OnUpdate()
    {
        follower.position = Vector3.Lerp(follower.position, target.position + startOffset + Vector3.up * game.StartPos.y, translateSpeed * Time.deltaTime);
    }
}

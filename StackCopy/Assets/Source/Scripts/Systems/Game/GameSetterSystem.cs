using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetterSystem : GameSystem, IIniting
{
    public void OnInit()
    {
        UIManager.ActivatePanel(EPanel.Game);
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PointsCounterSystem : GameSystem, IIniting, IUpdating
{
    [SerializeField] private TMP_Text pointsCounterText;

    [SerializeField] private int currencyEveryPointsCnt = 10;

    [SerializeField] private float ColorAngleChange = 5f;

    int clicksCount;

    int currencyGiven = 0;

    CurrencyUI CurrencyUI;

    public void OnInit()
    {
        var panel = UIManager.ActivatePanelAdditionally(EPanel.Currency);

        CurrencyUI = panel as CurrencyUI;
    }

    public void OnUpdate()
    {
        if (game.Status.IsOver)
            return;

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            ++clicksCount;

            player.Angle += ColorAngleChange;

            pointsCounterText.text = clicksCount.ToString();
        }

        if (clicksCount / currencyEveryPointsCnt > currencyGiven)
        {
            currencyGiven++;

            player.Money++;

            CurrencyUI.SetCurrencyCount(player.Money);
        }
    }
}

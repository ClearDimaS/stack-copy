using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;


public class ResultsSystem : GameSystem, IIniting
{
    [SerializeField] private Transform setParentNull;

    [SerializeField] private EventTrigger eventTrigger;

    [SerializeField] private float timeToMoveCam = 1f;

    public void OnInit()
    {
        GodManager.SavePlayer(player);

        UIManager.ActivatePanel(EPanel.Result);

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => { OnPointerDownDelegate((PointerEventData)data); });
        eventTrigger.triggers.Add(entry);

        setParentNull.parent = null;

        OffsetFov();
    }

    private void OffsetFov()
    {
        Camera camera = Camera.main;

        MeshRenderer[] meshRenderers = new MeshRenderer[2];

        if (game.LastCube == null)
            return;

        meshRenderers[0] = game.LastCube.GetComponentInChildren<MeshRenderer>();

        meshRenderers[1] = game.FirstCube.GetComponentInChildren<MeshRenderer>(); 

        Bounds bounds = GetMaxBounds(meshRenderers);

        float cameraDistance = 1.3f; // Constant factor
        Vector3 objectSizes = bounds.max - bounds.min;
        float objectSize = Mathf.Max(objectSizes.x, objectSizes.y, objectSizes.z);
        float cameraView = 2.0f * Mathf.Tan(0.5f * Mathf.Deg2Rad * camera.fieldOfView); // Visible height 1 meter in front
        float distance = cameraDistance * objectSize / cameraView; // Combined wanted distance from the object
        distance += 0.5f * objectSize; // Estimated offset from the center to the outside of the object
        Vector3 posToSeeAll = bounds.center - distance * camera.transform.forward;
        camera.transform.DOMove(posToSeeAll, timeToMoveCam);
    }

    public void OnPointerDownDelegate(PointerEventData pointerEventData)
    {
        //Bootstrap.GetSystem<AudioSysytem>().SetMusicLoopVolume(0.25f);
        //Signals.Get<PlaySoundSignal>().Dispatch(AudioSysytem.ClipType.MenuButton);

        SceneManager.LoadScene($"Start");
    }

    Bounds GetMaxBounds(Renderer[] renderers)
    {
        if (renderers.Length == 0) return new Bounds(transform.position, Vector3.zero);

        var b = renderers[0].bounds;

        foreach (Renderer r in renderers)
        {
            b.Encapsulate(r.bounds);
        }
        return b;
    }
}

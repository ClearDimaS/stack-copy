using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartColorAssigner : GameSystem, IIniting
{
    [SerializeField] private Material skyBoxMat;

    [SerializeField] private MeshRenderer fullColor;

    [SerializeField] private MeshRenderer gradient;

    [SerializeField] private float atten = 0.8f;

    [SerializeField] private float widthOfColorsAnglePerLevel = 120f;

    public void OnInit()
    {
        float minLevel = UnityEngine.Random.Range(0, 360f - widthOfColorsAnglePerLevel);

        float maxLevel = minLevel + widthOfColorsAnglePerLevel;

        game.ColorWheelToCol = new ColorWheelToCol(atten, minLevel, maxLevel);

        Color col = game.ColorWheelToCol.GetColor(player.Angle);

        skyBoxMat.SetColor($"_SkyColor1", game.ColorWheelToCol.GetColor(0f)) ;
        skyBoxMat.SetColor($"_SkyColor2", game.ColorWheelToCol.GetColor(180f));
        skyBoxMat.SetColor($"_SkyColor3", game.ColorWheelToCol.GetColor(360f));

        fullColor.sharedMaterial.SetColor("_Color", col);

        gradient.sharedMaterial.SetColor("_ColorTop", col);

        Color colNoAlpha = col;

        colNoAlpha.a = 0;

        gradient.sharedMaterial.SetColor("_ColorBot", colNoAlpha);
    }
}

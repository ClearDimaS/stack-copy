using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InfoSetterSystem : GameSystem, IIniting
{
    [SerializeField] private EventTrigger eventTrigger;

    public void OnInit()
    {
        UIManager.ActivatePanel(EPanel.PreGame);

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) => { OnPointerDownDelegate((PointerEventData)data); });
        eventTrigger.triggers.Add(entry);

        var panel = UIManager.ActivatePanelAdditionally(EPanel.Currency);

        var currencyUI = panel as CurrencyUI;

        currencyUI.SetCurrencyCount(player.Money);
    }

    public void OnPointerDownDelegate(PointerEventData pointerEventData)
    {
        //Bootstrap.GetSystem<AudioSysytem>().SetMusicLoopVolume(0.25f);
        //Signals.Get<PlaySoundSignal>().Dispatch(AudioSysytem.ClipType.MenuButton);

        GodManager.ChangeGameState(EGameState.Game);
    }
}

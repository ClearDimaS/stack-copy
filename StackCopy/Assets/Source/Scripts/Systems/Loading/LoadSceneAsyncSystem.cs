using NaughtyAttributes;
using Supyrb;
using System;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneAsyncSystem : GameSystem, IIniting
{
    [SerializeField]
    private GameObject[] dontDetroyOnLoadArr;

    //[SerializeField] private bool useCustomLevel;

    [SerializeField] private int periodToChangeVisual = 4;

  //  [SerializeField] private VisualData[] visualDataArr;

    public void OnInit()
    {
//#if UNITY_EDITOR
//        if (useCustomLevel)
//        {
//            player.CurrentLevel = startLevelNumber;
//            useCustomLevel = false;
//        }
//#endif

        foreach (GameObject go in dontDetroyOnLoadArr)
        {
            SetUnDestroyableOnLoad(go);
        }

        IEnumerator loadSceneCoroutine = LoadSceneAsync(OnVisualChanged);

        StartCoroutine(loadSceneCoroutine);
    }

    private void SetUnDestroyableOnLoad(GameObject go)
    {
        DontDestroyOnLoad(go);
    }

    private void SetDestroyableOnLoad(GameObject go)
    {
        SceneManager.MoveGameObjectToScene(go, SceneManager.GetActiveScene());
    }

    private IEnumerator LoadSceneAsync(Action onNewSceneLoadedCallback = null)
    {
        var loading = SceneManager.LoadSceneAsync($"sampleScene");

        while (loading.isDone == false)
        {
            yield return null;
        }

        onNewSceneLoadedCallback?.Invoke();
    }

    private void OnVisualChanged()
    {
        foreach (GameObject go in dontDetroyOnLoadArr)
        {
            SetDestroyableOnLoad(go);
        }

        GodManager.ChangeGameState(EGameState.Menu);
    }
}

[System.Serializable]
public class VisualData
{
    public Material[] materials;

    public string sceneName;

    public bool groundHasCollider;
}

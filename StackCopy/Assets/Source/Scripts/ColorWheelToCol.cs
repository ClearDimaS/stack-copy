
using UnityEngine;

public class ColorWheelToCol
{
    public ColorWheelToCol(float atten, float lowLimit, float highLimit)
    {
        this.atten = atten;

        this.lowLimit = lowLimit;

        this.highLimit = highLimit;
    }

    float atten = 0.7f;

    float lowLimit;

    float highLimit;

    public Color GetColor(float correctedANgle)
    {
        int times = (int)(correctedANgle / 360f);

        correctedANgle -= 360f * times;

        if (times % 2 == 1)
            correctedANgle = 360f - correctedANgle;

        correctedANgle = lowLimit + correctedANgle / 360f * (highLimit - lowLimit);

        correctedANgle = Mathf.Clamp(correctedANgle, lowLimit, highLimit);

        Color currentColor = Color.white;

        if (correctedANgle <= 60)
        {
            currentColor.r = 1f;
            currentColor.g = correctedANgle / 60f;
            currentColor.b = 0f;
        }
        else if (correctedANgle <= 120)
        {
            currentColor.r = 1f - (correctedANgle - 60) / 60f;
            currentColor.g = 1f;
            currentColor.b = 0f;
        }
        else if (correctedANgle <= 180)
        {
            currentColor.r = 0f;
            currentColor.g = 1f;
            currentColor.b = (correctedANgle - 120f) / 60f;
        }
        else if (correctedANgle <= 240)
        {
            currentColor.r = 0f;
            currentColor.g = 1f - (correctedANgle - 180f) / 60f;
            currentColor.b = 1;
        }
        else if (correctedANgle <= 300)
        {
            currentColor.r = (correctedANgle - 240f) / 60f;
            currentColor.g = 0f;
            currentColor.b = 1;
        }
        else //if (correctedANgle <= 360)
        {
            currentColor.r = 1;
            currentColor.g = 0f;
            currentColor.b = 1f - (correctedANgle - 300f) / 60f;
        }

        currentColor = Color.Lerp(Color.white, currentColor, atten);

        return currentColor;
    }
}
